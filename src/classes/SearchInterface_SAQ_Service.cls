public class SearchInterface_SAQ_Service {
    
    public List<Object> InitAPI(){
        Http h1 = new Http();   
        HttpResponse httpResp = h1.send(getHttpReq('GET',null));
       	return parseHTTPResponse(httpResp);
    }
    
    public List<Object> searchByString(String searchString){
        Http h1 = new Http(); 
        //String payLoad = '{"q":"' + searchString + '"}';
        HttpResponse httpResp = h1.send(getHttpReq('GET',searchString));
       	return parseHTTPResponse(httpResp);
    }
    
    public List<Object> parseHTTPResponse(HttpResponse res1){
        List<Object> jsonOutputResult = new List<Object>();
        if(res1.getStatusCode() == 200){
            try{
                //SAQ_WSEntity entityDeserialized = (SAQ_WSEntity)JSON.deserialize(res1.getBody(),SAQ_WSEntity.class);
                Map<String,Object> jsonOutput = (Map<String, Object>)JSON.deserializeUntyped(res1.getBody());
                jsonOutputResult = (List<Object>)jsonOutput.get('results');
				
            }
            Catch(Exception ex){
                System.debug(ex.getStackTraceString());
            }
        }
        return jsonOutputResult;
    }
    public HttpRequest getHttpReq(string httpVerb,String httpBody){
        HttpRequest req1 = new HttpRequest();
        
        req1.setMethod(httpVerb);
        //req1.setHeader('Authorization', 'Bearer ' + Label.Access_Token_SAQ );
        if(!String.isEmpty(httpBody)){
            req1.setEndpoint('https://cloudplatform.coveo.com/rest/search?access_token=' +Label.Access_Token_SAQ + '&q='+ httpBody );
        }
        else{
            req1.setEndpoint('https://cloudplatform.coveo.com/rest/search?access_token=' +Label.Access_Token_SAQ );
        }
        return req1;
    }
    
}