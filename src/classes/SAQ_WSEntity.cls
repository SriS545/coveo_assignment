public with sharing class SAQ_WSEntity {
	Integer totalCount {get;set;}
  	Integer totalCountFiltered {get;set;}
  	Integer duration {get;set;}
  	Integer indexDuration {get;set;}
  	Integer requestDuration {get;set;}
  	String searchUid {get;set;}
  	String pipeline {get;set;}
  	Integer apiVersion {get;set;}
  	String usageBucket {get;set;}
  	String basicExpression {get;set;}
	String advancedExpression {get;set;}
	String largeExpression {get;set;}
	String constantExpression {get;set;}
	String disjunctionExpression {get;set;}
	String mandatoryExpression {get;set;}
	List<UserIdentities> userIdentities {get;set;} 
  	List<String> rankingExpressions {get;set;}
  	List<String> topResults {get;set;}
  	ExecutionReport executionReport {get;set;}
  	String index {get;set;}
  	String indexToken {get;set;}
  	List<String> refinedKeywords {get;set;}
  	List<String> triggers {get;set;}
  	//termsToHighlight : { }
  	//phrasesToHighlight : { }
  	List<String> queryCorrections {get;set;}
  	List<String> groupByResults	{get;set;}
  	List<String> categoryFacets {get;set;}
  	List<SearchResults> results {get;set;}
      


      public Class UserIdentities{
      	String name {get;set;}
	    String provider {get;set;}
	    String type {get;set;}
      }
      public Class ExecutionReport{
      	Integer duration {get;set;}
      	List<ExceptionReportChildren> children{get;set;}
      }

      public Class ExceptionReportChildren{
      	String name {get;set;}
      	String description {get;set;}
      	Integer duration {get;set;}
      	String X_Request_ID {get;set;}
      	String coveo_organization {get;set;}
      	ExceptionReportChildrenconfigured configured {get;set;}
      }
      public Class ExceptionReportChildrenconfigured{
      	String primary {get;set;}
      	List<String> secondary {get;set;}
      	List<String> mandatory {get;set;}
      }

      public Class SearchResults{
      	String title {get;set;}
    	String uri {get;set;}
    	String printableUri {get;set;}
    	String clickUri {get;set;}
    	String uniqueId {get;set;}
    	String excerpt {get;set;}
    	String firstSentences {get;set;}
	    String summary {get;set;}
	    String flags {get;set;}
	    Boolean hasHtmlVersion {get;set;}
	    Boolean hasMobileHtmlVersion {get;set;}
	    Integer score {get;set;}
	    Decimal percentScore {get;set;}
	    String rankingInfo {get;set;}
	    Decimal rating {get;set;}
	    Boolean isTopResult {get;set;}
	    Boolean isRecommendation {get;set;}
	    List<String> titleHighlights {get;set;}
	    List<String> firstSentencesHighlights {get;set;}
	    List<String> excerptHighlights {get;set;}
	    List<String> printableUriHighlights {get;set;}
	    List<String> summaryHighlights {get;set;}
	    String parentResult {get;set;}
	    List<String> childResults {get;set;}
	    Integer totalNumberOfChildResults {get;set;}
	    List<String> absentTerms {get;set;}
	    /* String Title {get;set;}
    	String Uri {get;set;}
	    String PrintableUri {get;set;}
	    String ClickUri {get;set;}
	    String UniqueId {get;set;}
	    String Excerpt {get;set;}
	    String FirstSentences {get;set;}
	    List<SearchResultsRaw> raw {get;set;} */
      }

      public Class SearchResultsRaw{
      	String systitle {get;set;}
      	String systopparent {get;set;}
	    String tptemperaturedeservice {get;set;}
	    String sysurihash {get;set;}
	    String urihash {get;set;}
	    String tpdisponibiliteraw {get;set;}
	    String tpobservationsgustativescorps {get;set;}
	    String tpcepagenomsplitgroup {get;set;}
	    String tpbouchon {get;set;}
	    String tpcodecup {get;set;}
	    String sysuri {get;set;}
	    String sysprintableuri {get;set;}
	    String sysfolders {get;set;}
	    String tppotentieldegarde {get;set;}
	    String tpproducteur {get;set;}
	    String tpobservationsgustativestexture {get;set;}
	    String tpcategorieraw {get;set;}
	    String tpcategorie {get;set;}
	    String sysconcepts {get;set;}
	    String tpinventairetypenomsuccursalerawsplitgroup {get;set;}
	    String tpaccordstumbnailuriraw {get;set;}
	    String tpobservationsolfactivesaromessplitgroup {get;set;}
	    String tpfamilledevin {get;set;}
	    String sysindexeddate {get;set;}
	    String tpcepagesplitgroup {get;set;}
	    String tpnotededegustation {get;set;}
	    String syslanguage {get;set;}
	    String tpinventairenomsuccursalesplitgroup {get;set;}
	    String tpprixbande {get;set;}
	    String tpcoveoconnectorhasbinarydata {get;set;}
	    String pmillesimedeguste {get;set;}
	    String tpregion {get;set;}
	    String tppays {get;set;}
	    String tpdisponibilite {get;set;}
	    String tpaccordsnommenu {get;set;}
	    String tpinventairetypesuccursalesplitgroup {get;set;}
	    String tpcouleur {get;set;}
	    String syssite {get;set;}
	    String tppagetitle {get;set;}
	    String tpthumbnailuri {get;set;}
	    Decimal tpprixnum {get;set;}
	    String sysdocumenttype {get;set;}
	    String tpprixnormal {get;set;}
	    String syssource {get;set;}
	    String tpinventaireenligne {get;set;}
	    Double syssize {get;set;}
	    Double sysdate {get;set;}
	    String tppagebody {get;set;}
	    String tpmillesime {get;set;}
	    String tpnomdebouteille {get;set;}
	    String tpformat {get;set;}
	    String systopparentid {get;set;}
	    String tpobservationsgustavivesfamillesaromessplitgroup {get;set;}
	    String tpcodesaq {get;set;}
	    String syssourcetype {get;set;}
	    String tpobservationsgustativesacidite {get;set;}
	    String sysclickableuri {get;set;}
	    String sysfiletype {get;set;}
	    String tpcontenant {get;set;}
	    String tpproductid {get;set;}
	    Double sysrowid {get;set;}
	    String syscollection {get;set;}
      	String tpfamilledevinsplitgroup {get;set;}
      }

		public SAQ_WSEntity() {
		
		}
}