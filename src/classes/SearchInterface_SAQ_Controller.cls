public with sharing class SearchInterface_SAQ_Controller {
    
    
    @AuraEnabled
    public Static Object[] doApiActions(){
        SearchInterface_SAQ_Service srvObj = new SearchInterface_SAQ_Service();
        return srvObj.InitAPI();
    }
    @AuraEnabled
    public Static Object[] SearchByString(String searchKey){
        if(!String.isEmpty(searchKey)){
            SearchInterface_SAQ_Service srvObj = new SearchInterface_SAQ_Service();
            return srvObj.searchByString(searchKey);
        }
        else
            return null;
    }
}