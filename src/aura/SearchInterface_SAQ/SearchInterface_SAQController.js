({
	getAlchoholsList : function(component, event, helper) {
		helper.getAlchoholsList(component,event);
        var lstCountry = ['-- none --','France', 'Italie', 'États-Unis', 'Canada', 'Espagne', 'Royaume-Uni',
                        'Portugal', 'Chili', 'Australie', 'Afrique du Sud', 'Argentine', 'Nouvelle-Zélande',
                        'Mexique', 'Allemagne', 'Grèce', 'Autriche', 'Irlande', 'Japon', 'Belgique',
                        'Liban', 'Hongrie', 'Israël', 'Suisse', 'Suède', 'Pays-Bas',
                        'Pologne', 'Guyana', 'Jamaïque', 'République dominicaine', 'Russie', 'Colombie',
                        'Barbade', 'République tchèque', 'Uruguay', 'Martinique', 'Trinidad et Tobago',
                        'Porto Rico', 'Chine', 'Roumanie', 'Brésil', 'Pérou', 'Panama', 'Guatemala',
                        'Cuba', 'Sainte-Lucie', 'la République de Corée de', 'Maurice', 'la Moldavie',
                        'Venezuela', 'Bulgarie', 'Arménie (République de)', 'Haïti', 'Luxembourg',
                        'Nicaragua', 'Lettonie', 'Taiwan', 'Tunisie', 'Danemark', 'Inde', 'Maroc',
                        'Grenade', 'Thaïlande', 'Guadeloupe', 'Guyane française', 'El Salvador',
                        'Slovaquie', 'Turquie', 'Géorgie', 'Norvège', 'Belize', 'Finlande', 'Algérie',
                        'Croatie', 'Ukraine', 'Islande', 'Madagascar', 'Macédoine', 'Mongolie', 'Îles Féroé'];
        var lstYear = ['-- none --','2016','2015','2017','2014','2013','2012','2011','2010','2009','2006','2008','2005',
                     '2018','2007','2004','2000','2003','2002','1999','1998','1990','2001','1989','1996','1997','1995',
                     '1986','1967','1978','1769','1800','1977','1975','1988','1969','1971','1902','1994','1821','1984',
                     '1991','1665','1973','1993','1982','1972','1981','1976','1963','1970','1985','1987','1894','1966',
                     '1824','1980','1965','1961','1796','1888','1935','1983','1951','1944','1840','1968','1792','1931',
                     '1955','1896','1573','1919','1964','1270','1509','1859','1979','1942','1912','1906','1974','1937',
                     '1918','1922','1914','1108','1534','1312','1738','1880'];
        var catList=['-- none --','Vins rouges', 'Vin blanc', 'Vin rosé', 'Whisky écossais', 'Vin mousseux',
                     'Whiskey américain', 'Vin de glace', 'Vin de dessert', 'Porto', 'Cognac', 'Tequila',
                     'Vin mousseux rose', 'Madère', 'Brandy', 'Cave à vin', 'Vodka', 'Rhum ambré', 'Rhum agricole',
                     'Dry gin', 'Calvados Pays d’Age', 'Liquor', 'Sangria', 'Armagnac', 'Bière blonde de type Lager',
                     'Bière noire de type Ale', 'Vin d\'apéritif', 'Vin fortifié', 'Eau de vie aromatisée'];
        var priceList = ['-- none --','<10','10-20','20-30','30-40','40-50','50-60','60-70','70-80','80-90',
                        '90-100','100-125','125-150','150-175','175-200','200-300','300-400','400-500','500-600',
                         '600-700','700-800','800-900','900+'];
        
        component.set('v.countryList', lstCountry);
        component.set('v.yearList', lstYear);
        component.set('v.catList', catList);
        component.set('v.priceList', priceList);
	},
    handleSearch: function(component, event, helper) {
        if(event.keyCode == 13){
        	helper.getItemsBySearch(component,event);
            //console.log(component.find('enter-search').get('v.value'));
        }
		
	},
    handleChange: function(component, event, helper) {
        if(component.find('enter-search').get('v.value') == ''){
        	helper.getAlchoholsList(component,event);
        }
	},
})