({
	getAlchoholsList : function(cmp,e) {
		var action = cmp.get("c.doApiActions");
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.debug(response.getReturnValue().length);
                cmp.set("v.apiResults", response.getReturnValue());
            } else {
                console.debug(response);
            }
            //cmp.set('v.issearching', false);
        });
	 $A.enqueueAction(action);
	},
    getItemsBySearch : function(cmp,e){
        var action = cmp.get("c.SearchByString");
        cmp.set('v.issearching', true);
        action.setParams({searchKey : cmp.find('enter-search').get('v.value')});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.debug(response.getReturnValue().length);
                cmp.set("v.apiResults", response.getReturnValue());
            } else {
                console.debug(response);
            }
            cmp.set('v.issearching', false);
        });
	 $A.enqueueAction(action);
    }
})